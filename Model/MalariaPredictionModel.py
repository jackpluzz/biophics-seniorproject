#!/usr/bin/env python
# coding: utf-8

# In[1]:


#LOAD THE DATA FROM MariaDB SELECT ID 1-500
from sqlalchemy import create_engine
import pandas as pd
db_connection_str = create_engine('mysql+pymysql://root:@localhost/malaria_spicies_predict')
df = pd.read_sql('SELECT * FROM malaria_information_model', con=db_connection_str)
# WHERE id BETWEEN 1 AND 500
print('Loade Data For Train : Success')

#df.head()


#Data Preparation
numeric = df[['Age']]


df_cat = df[['bloddraw_year','bloddraw_day','site_district','occupation_id']]

df_cat = df_cat.astype(str)

df_cat2 = df[['bloddraw_month','sex','nationality','Peopletype']]

class_label = df[['result_1']]

nominal_data = pd.concat([df_cat, df_cat2], axis=1)
nominal_data.head()

nominal_data = pd.get_dummies(nominal_data)
nominal_data.head()

preprocessed_data = pd.concat([numeric,nominal_data], axis=1)
preprocessed_data.head()

from sklearn.preprocessing import MinMaxScaler
mms = MinMaxScaler()
norm_data = mms.fit_transform(preprocessed_data)
norm_data=pd.DataFrame(norm_data, columns=preprocessed_data.columns) 
norm_data.head()

print('Preprocessing data for train : Success')

import pandas as pd
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn import tree
from sklearn.externals.six import StringIO  
from IPython.display import Image  
import pydotplus
import pickle
from sqlalchemy import create_engine


#Seperate data attributes (X) 
X = norm_data.values

#Seperate class column (Y)
Y = class_label.values 



from sklearn.decomposition import PCA
pca = PCA(n_components=5,random_state=0)
norm_data = pca.fit_transform(norm_data)



norm_data

import numpy as np
from collections import Counter
from imblearn.under_sampling import AllKNN
allknn = AllKNN(n_neighbors=75,sampling_strategy='majority')
print('Original datavb set shape %s' % Counter(class_label))
X_res, y_res = allknn.fit_resample(norm_data, class_label)
print('Resampled dataset shape %s' % Counter(y_res))

from sklearn.model_selection import train_test_split
#Split training set and test set with ration 80% : 20%
x_train, x_test, y_train, y_test = train_test_split(X_res,y_res, random_state = 0, test_size = 0.20)


# see the shape of train and test sets
print("x_train shape: {}\ny_train shape: {}".format(x_train.shape, y_train.shape))
print("x_test shape: {}\ny_test shape: {}".format(x_test.shape, y_test.shape))

print('Re imbalanced data : Success')

#K-Nearest Neighbors Classifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics
from sklearn.metrics import accuracy_score
from sklearn.model_selection import cross_val_score

# initialize the knn model with k = 1
knn = KNeighborsClassifier(n_neighbors=1, metric = 'euclidean')
#Fit training data to classifier
knn.fit(x_train,y_train)

predictions = knn.predict(x_test)
### Accuracy
accuracy = accuracy_score(y_true = y_test, y_pred = predictions)

print(accuracy)
scores = cross_val_score(estimator=knn, X=x_train, y=y_train, cv=10, n_jobs=1, scoring='accuracy')
print('CV accuracy: %.3f +/- %.3f' % (scores.mean(), scores.std()))

filename= "D:\\@Work\\SeniorProject\\Malaria-SP\\Model\\malaria_prediction_model.sav"
pickle.dump(knn, open(filename, 'wb'))

print('Training model : Success')

