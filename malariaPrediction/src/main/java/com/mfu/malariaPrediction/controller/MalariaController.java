package com.mfu.malariaPrediction.controller;

import com.mfu.malariaPrediction.CsvUtil;
import com.mfu.malariaPrediction.entity.MalariaInformationForModel;
import com.mfu.malariaPrediction.entity.MalariaInformationForPredict;
import com.mfu.malariaPrediction.entity.ResultMalariaPrediction;
import com.mfu.malariaPrediction.service.MalariaService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.exec.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin
@Slf4j
public class MalariaController {
    @Autowired
    MalariaService malariaService;

    // ChartData
    @GetMapping("/spicies")
    public ResponseEntity<?> getSpiciesData() {
        List<MalariaInformationForModel> malariaInformationForModelList = malariaService.getAllMalaria();
        return ResponseEntity.ok(malariaService.getSpiciesPieChartData(malariaInformationForModelList));
    }

    @GetMapping("/gender")
    public ResponseEntity<?> getGenderData() {
        List<MalariaInformationForModel> malariaInformationForModelList = malariaService.getAllMalaria();
        return ResponseEntity.ok(malariaService.getGenderPieChartData(malariaInformationForModelList));
    }

    @GetMapping("/monthdata")
    public ResponseEntity<?> getMonthData() {
        List<MalariaInformationForModel> malariaInformationForModelList = malariaService.getAllMalaria();
        return ResponseEntity.ok(malariaService.getMonthChart(malariaInformationForModelList));
    }

    //---------------------------------------------
    @PostMapping("/malaria/buildmodel")
    public ResponseEntity<?> buildmodel() throws IOException, InterruptedException {
        System.out.println("Build model Started!!");
        String path = "\"D:\\@Work\\SeniorProject\\Malaria-SP\\Model\\MalariaPredictionModel.py\"";
        log.info("path {}", path);
        CommandLine cmdLine = new CommandLine("python");
        cmdLine.addArgument(path);
        DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
        ExecuteWatchdog watchdog = new ExecuteWatchdog(60 * 10000);
        Executor executor = new DefaultExecutor();
        executor.setExitValue(1);
        executor.setWatchdog(watchdog);
        executor.execute(cmdLine, resultHandler);
        resultHandler.waitFor();

        return ResponseEntity.status(200).build();
    }


    //Import data for Test Malaria
    @PostMapping("/malaria/import")
    public ResponseEntity<?> importTestdata(@RequestParam("file") MultipartFile file) throws IOException, InterruptedException {
        List<MalariaInformationForPredict> malariaInformationForPredicts = CsvUtil.
                read(MalariaInformationForPredict.class, file.getInputStream());
        malariaService.saveAllMalariaInformation(malariaInformationForPredicts);
        System.out.println("Import Success");
        malariaService.saveAllMalariaInformation(malariaInformationForPredicts);
        String path = "\"D:\\@Work\\SeniorProject\\Malaria-SP\\Model\\malaria-prediction-r.py\"";
        log.info("path {}", path);
        CommandLine cmdLine = new CommandLine("python");
        cmdLine.addArgument(path);
        DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();

        ExecuteWatchdog watchdog = new ExecuteWatchdog(60 * 1000);
        Executor executor = new DefaultExecutor();
        executor.setExitValue(1);
        executor.setWatchdog(watchdog);
        executor.execute(cmdLine, resultHandler);
        resultHandler.waitFor();

        // TODO: add upload log

        return ResponseEntity.ok().build();

    }

    //-----------------------------------------
    // Import data for trainingSet
    @PostMapping("/model/import")
    public ResponseEntity<?> importdatamodel(@RequestParam("file") MultipartFile file) throws IOException, InterruptedException {
        System.out.println("Start Import data");
        List<MalariaInformationForModel> malariaInformationForModels = CsvUtil.
                read(MalariaInformationForModel.class, file.getInputStream());
        malariaService.saveAllDataModelInformation(malariaInformationForModels);
        System.out.println("Finished Import data");
        return ResponseEntity.ok().build();

    }
//--------------------------------------------------------------------
    // Export data from Result

    @GetMapping("/malaria")
    public ResponseEntity<?> exportCSV() {

        List<ResultMalariaPrediction> resultMalariaPredictions = malariaService
                .getAllMalariaInformation();

        malariaService.nukeAllData();

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=MalariaSpiciesPrediction.csv");
        return ResponseEntity.ok().headers(headers)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(CsvUtil.convertToCSV(resultMalariaPredictions));

    }

    //-----------------------------------------------------------------
    //Export data from Training model
    @GetMapping("/datamodel")
    public ResponseEntity<?> exportmodelCSV() {
        List<MalariaInformationForModel> malariaInformationForModels = malariaService
                .getAllDatamodel();
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=Malariadatatrain.csv");
        return ResponseEntity.ok().headers(headers)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(CsvUtil.convertToCSV(malariaInformationForModels));

    }

    @DeleteMapping("/nukemodel")
    public ResponseEntity<?> nukemodel() {
        malariaService.nukeModel();
        System.out.println("Nuke Model here");
        return ResponseEntity.status(200).build();

    }


//-----------------------------------------------------------------------------

//    @GetMapping(name = "/user")
//    public List<UserEntity> getAllUser(){
//        return this.userService.getAllUser();
//    }
//
//    @PostMapping(path = "/user/insert")
//    public UserEntity insertUser(@RequestBody @Valid UserEntity user) {
//        return userService.insertUser(user);
//    }


}
