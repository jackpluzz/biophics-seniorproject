package com.mfu.malariaPrediction.service;

import com.mfu.malariaPrediction.Model.ChartData;
import com.mfu.malariaPrediction.Model.ChartSeries;
import com.mfu.malariaPrediction.entity.MalariaInformationForModel;
import com.mfu.malariaPrediction.entity.MalariaInformationForPredict;
import com.mfu.malariaPrediction.entity.ResultMalariaPrediction;
import com.mfu.malariaPrediction.repository.MalariaModelRepository;
import com.mfu.malariaPrediction.repository.MalariaPredictionRepository;
import com.mfu.malariaPrediction.repository.ResultRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@Slf4j
@CrossOrigin
public class MalariaService {
    @Autowired
    MalariaPredictionRepository malariaPredictionRepository;
    @Autowired
    ResultRepository resultRepository;
    @Autowired
    MalariaModelRepository malariaModelRepository;


    public List<MalariaInformationForModel> getAllMalaria() {

        // log.info("data List {}",objList);
        return malariaModelRepository.findAll();
    }

    public List<ChartSeries> getSpiciesPieChartData(List<MalariaInformationForModel> malariaInformationForPredictList) {
        Map<String, Long> spiciesMap = malariaInformationForPredictList.stream().collect(Collectors.groupingBy(MalariaInformationForModel::getResult_1, Collectors.counting()));
        ChartSeries chartSeries = new ChartSeries();
        chartSeries.setName("Result");
        List<ChartData> chartDataList = new ArrayList<ChartData>();
        spiciesMap.forEach((result, count) -> {
            ChartData chartData = new ChartData();
            chartData.setName(result);
            chartData.setY(Double.valueOf(count));
            chartDataList.add(chartData);

        });
        chartSeries.setData(chartDataList);
        return Arrays.asList(chartSeries);

    }

    public List<ChartSeries> getGenderPieChartData(List<MalariaInformationForModel> malariaInformationForPredictList) {
        Map<String, Long> genderMap = malariaInformationForPredictList.stream().collect(Collectors.groupingBy(MalariaInformationForModel::getSex, Collectors.counting()));
        ChartSeries chartSeries = new ChartSeries();
        chartSeries.setName("Result");
        List<ChartData> chartDataList = new ArrayList<ChartData>();
        genderMap.forEach((gender, count) -> {
            ChartData chartData = new ChartData();
            chartData.setName(gender);
            chartData.setY(Double.valueOf(count));
            chartDataList.add(chartData);

        });
        chartSeries.setData(chartDataList);
        return Arrays.asList(chartSeries);

    }


    public List<ChartSeries> getMonthChart(List<MalariaInformationForModel> malariaInformationForModelList) {
        Map<String, Long> monthMap = malariaInformationForModelList.stream().collect(Collectors.groupingBy(MalariaInformationForModel::getBloddraw_month, Collectors.counting()));
        ChartSeries chartSeries = new ChartSeries();
        chartSeries.setName("Amount of Patient each month");
        chartSeries.setDataSorting("enabled: true");
        List<ChartData> chartDataList = new ArrayList<ChartData>();
        monthMap.forEach((month, count) -> {
            ChartData chartData = new ChartData();
            chartData.setName(month);
            chartData.setY(Double.valueOf(count));
            chartData.setName(month);
            chartDataList.add(chartData);

        });

        log.info("data {}", chartDataList);
        chartSeries.setData(chartDataList);
        return Arrays.asList(chartSeries);
    }
//    public ChartOptionResponse getMonthData(List<MalariaInformationForModel> malariaInformationForModelList) {
//        ChartOptionResponse optionResponse = new ChartOptionResponse();
//        List<String> x = malariaInformationForModelList.stream().map(MalariaInformationForModel::getBloddraw_month).distinct().sorted().collect(Collectors.toList());
//        Map<String, Long> MonthMap = malariaInformationForModelList.stream().collect(Collectors.groupingBy(MalariaInformationForModel::getBloddraw_month, Collectors.counting()));
//
//        List<ChartSeries> chartSeriesList = new ArrayList<>();
//
//        MonthMap.forEach((month, count) -> {
//            ChartSeries chartSeries = new ChartSeries();
//            chartSeries.setName(month);
//
//            chartSeries.setData(chartSeriesList);
//            chartSeriesList.add(chartSeries);
//        });
//        XAxis xAxis = new XAxis();
//        xAxis.setCategories(x);
//        optionResponse.setSeries(chartSeriesList);
//        optionResponse.setXAxis(xAxis);
//        return optionResponse;
//    }


    public void saveAllMalariaInformation(List<MalariaInformationForPredict> malariaInformationForPredicts) {
        malariaPredictionRepository.saveAll(malariaInformationForPredicts);
    }

    public void saveAllDataModelInformation(List<MalariaInformationForModel> malariaInformationForModels) {
        malariaModelRepository.saveAll(malariaInformationForModels);
    }

    public List<ResultMalariaPrediction> getAllMalariaInformation() {
        return resultRepository.findAll();
    }

    public List<MalariaInformationForModel> getAllDatamodel() {
        return malariaModelRepository.findAll();
    }

    public void nukeAllData() {

        resultRepository.deleteAll();
        malariaPredictionRepository.deleteAll();


    }


    public void nukeModel() {
        malariaModelRepository.deleteAll();
    }


}
