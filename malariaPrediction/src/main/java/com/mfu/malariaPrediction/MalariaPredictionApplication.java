package com.mfu.malariaPrediction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MalariaPredictionApplication {

	public static void main(String[] args) {
		SpringApplication.run(MalariaPredictionApplication.class, args);
	}

}
