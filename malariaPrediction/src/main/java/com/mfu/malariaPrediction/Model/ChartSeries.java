package com.mfu.malariaPrediction.Model;

import lombok.Data;

import java.util.List;

@Data
public class ChartSeries {
    private String name;
    private List<?> data;
    private String dataSorting;

}
