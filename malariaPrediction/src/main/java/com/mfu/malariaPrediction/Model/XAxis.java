package com.mfu.malariaPrediction.Model;

import lombok.Data;
import java.util.List;

@Data
public class XAxis {
    private List<String> categories;
}