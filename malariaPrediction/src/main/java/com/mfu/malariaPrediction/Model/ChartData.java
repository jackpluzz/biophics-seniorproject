package com.mfu.malariaPrediction.Model;

import lombok.Data;

@Data
public class ChartData {
    private String name;
    private Double y;
}
