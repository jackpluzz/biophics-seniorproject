package com.mfu.malariaPrediction.Model;

import lombok.Data;

import java.util.List;

@Data
public class ChartOptionResponse {
    private XAxis xAxis;
    private List<ChartSeries> series;
}
