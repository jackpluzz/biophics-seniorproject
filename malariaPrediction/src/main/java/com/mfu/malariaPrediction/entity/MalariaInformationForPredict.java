package com.mfu.malariaPrediction.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "malaria_information_for_predict")
public class MalariaInformationForPredict {
    @Id
    @Column(name = "id")
    private int id;

    @Column(name = "bloddraw_year")
    private int bloddraw_year;

    @Column(name = "bloddraw_month")
    private String bloddraw_month;

    @Column(name = "bloddraw_day")
    private String bloddraw_day;

    @Column(name = "Age")
    private int age;

    @Column(name = "sex")
    private String sex;

    @Column(name = "nationality")
    private String nationality;

    @Column(name = "site_district")
    private String site_district;

    @Column(name = "occupation_id")
    private String occupation_id;

    @Column(name = "Peopletype")
    private String Peopletype;


}
