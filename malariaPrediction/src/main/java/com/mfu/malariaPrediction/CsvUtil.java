package com.mfu.malariaPrediction;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class CsvUtil {
    private static final CsvMapper mapper = new CsvMapper();

    public static <T> List<T> read(Class<T> clazz, InputStream stream) throws IOException {
        CsvSchema schema = mapper.schemaFor(clazz).withHeader().withColumnReordering(true);
        ObjectReader reader = mapper.readerFor(clazz).with(schema);
        return reader.<T>readValues(stream).readAll();
    }

    public static <T> byte[] convertToCSV(List<T> objectList) {

        if (objectList.isEmpty())
            return null;

        T entry = objectList.get(0);

        char delimiter = ',';
        char quote = '"';
        String lineSep = "\n";

        CsvMapper mapper = new CsvMapper();
        mapper.disable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY);

        CsvSchema schema = mapper.schemaFor(entry.getClass());

        try {
            return mapper.writer(schema
                    .withColumnSeparator(delimiter)
                    .withQuoteChar(quote)
                    .withLineSeparator(lineSep)
                    .withHeader()).writeValueAsBytes(objectList);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return new byte[0];
    }
}
