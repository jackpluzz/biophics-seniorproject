package com.mfu.malariaPrediction.repository;

import com.mfu.malariaPrediction.entity.MalariaInformationForPredict;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MalariaPredictionRepository extends JpaRepository<MalariaInformationForPredict,String> {


}
