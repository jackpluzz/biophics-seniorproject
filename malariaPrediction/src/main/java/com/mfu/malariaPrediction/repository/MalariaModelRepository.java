package com.mfu.malariaPrediction.repository;

import com.mfu.malariaPrediction.entity.MalariaInformationForModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MalariaModelRepository extends JpaRepository<MalariaInformationForModel,String> {
  //  List<MalariaInformationForModel> findAllByOrderByBloddrawMonthAsc();
}
