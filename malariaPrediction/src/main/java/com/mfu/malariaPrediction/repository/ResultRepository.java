package com.mfu.malariaPrediction.repository;

import com.mfu.malariaPrediction.entity.ResultMalariaPrediction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResultRepository extends JpaRepository<ResultMalariaPrediction,String> {
}
