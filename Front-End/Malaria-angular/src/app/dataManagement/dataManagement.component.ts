import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { saveAs } from 'file-saver';
import { UserRole } from './../models/user.model'
import { User } from '@app/_models';
import { UserService } from '@app/_services';
import { ApiService } from '@app/service/Api.service';
import { from } from 'rxjs';

@Component({ templateUrl: 'dataManagement.component.html' })
export class dataManagementComponent implements OnInit {
    loading = false;
    users: User[] = [];
    userList: UserRole[] = [];
    userObject: UserRole = new UserRole();
    fileName: string = "Choose file"
    spinStatus = false;
    spinModelStatus = false;
    btnSubmitStatus = true;
    btnModelStatus = true;
    btnDeleteStatus = true;
    spinDeleteStatus = false;
    constructor(private userService: UserService, private apiservice: ApiService) { }

    ngOnInit() {
        this.reset()
        this.loading = true;
        this.userService.getAll().pipe(first()).subscribe(users => {
            this.loading = false;
            this.users = users;
        });
    }

    exportcsv() {
        this.apiservice.exportdatamodelcsv().subscribe(res => {
            const blob = new Blob([res], { type: 'text/csv' });
            saveAs(blob, 'Malaria-Training set.csv')
        });
    }

    fileToUpload: File;
    handleFile(files: FileList) {
        this.fileToUpload = files.item(0);
        this.fileName = this.fileToUpload.name
    }

    importcsv() {
        this.spinStatus = true;
        this.btnSubmitStatus = false;
        this.apiservice.importdatamodelcsv(this.fileToUpload).subscribe(data => {
            alert("success")
            window.location.reload();
            this.btnSubmitStatus = true;
            this.spinStatus = false;
        }, error => {
            alert('Upload Fail!, Please Check data column')
            this.spinStatus = false;
            this.btnSubmitStatus = true;
        });
    }

    trainmodel() {
        this.spinModelStatus = true;
        this.btnModelStatus = false;
        this.apiservice.trainmodel().subscribe(res => {
            alert('Train model success')
            window.location.reload();
            this.spinModelStatus = false;
            this.btnModelStatus = true;
        }, error => {
            alert('Fail to Training Model Please try agian!')
            this.spinModelStatus = false;
            this.btnModelStatus = true;
        })
    }

    nukemodel() {
        this.btnDeleteStatus = false;
        this.spinDeleteStatus = true;
        this.apiservice.nukemodel().subscribe(res => {
            alert('Delete Train model success')
            window.location.reload();
            this.btnDeleteStatus = true;
            this.spinDeleteStatus = false;
        }, error => {
            alert('Fail to Delete data training, Please try again')
            this.spinDeleteStatus = false;
            this.btnDeleteStatus = true;
        })
    }
    reset() {
        this.fileToUpload = undefined;
        this.fileName = "Choose file";
        this.spinDeleteStatus = false;
        this.spinModelStatus = false;
        this.btnSubmitStatus = false;
        this.btnSubmitStatus = true;
        this.btnModelStatus = true;
        this.btnDeleteStatus = true;
    }

}