﻿import { Component } from '@angular/core';
import { first, flatMap } from 'rxjs/operators';
import { Chart } from 'angular-highcharts';
import { User } from '@app/_models';
import { UserService, AuthenticationService } from '@app/_services';
import { saveAs } from 'file-saver';
import { ApiService } from '../service/Api.service'
@Component({ templateUrl: 'home.component.html' })
export class HomeComponent {
    loading = false;
    spinStatus = false;
    btnDownloadStatus = false;
    btnSubmitStatus = true;
    currentUser: User;
    userFromApi: User;
    chart: Chart;
    chartGender: Chart;
    chartMonth: Chart;
    fileToUpload:File;
    fileName:string = "Choose file"
    constructor(
        private userService: UserService,
        private authenticationService: AuthenticationService,
        private chartService: ApiService
    ) {
        this.currentUser = this.authenticationService.currentUserValue;
    }

    ngOnInit() {
        this.reset()
        this.loading = true;
        this.userService.getById(this.currentUser.id).pipe(first()).subscribe(user => {
            this.loading = false;
            this.userFromApi = user;
        });

        this.chartService.getMonth().subscribe(res => {
            console.log(res);
            let chartMonth = new Chart({
                chart: {
                    type: 'column'
                },
                title: { text: 'Number of Patient in each month' },
                xAxis: {
                    type: 'category',


                },
                yAxis: {
                    title: {
                        text: 'Total Patients'
                    }
                },
                legend: {
                    enabled: false
                },

                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y} People'
                        },

                    }
                }, tooltip: {
                    headerFormat: '<span style="font-size:11px">Amount of Patient</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
                },
                series: res
            })
            this.chartMonth = chartMonth;
        })

        this.chartService.getSpicies().subscribe(res => {
            console.log(res);
            let chart = new Chart({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },

                title: {
                    text: 'Malaria Spicies Ratio'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                        },
                        showInLegend: true
                    }
                },
                series: res
            });
            this.chart = chart;
        });

        this.chartService.getGender().subscribe(res => {
            console.log(res);
            let chart2 = new Chart({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Gender Ratio'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                        },
                        showInLegend: true
                    }
                },
                series: res
            });
            this.chartGender = chart2;
        });

    }

    // handleFile(evt) {
    //     console.log("evt")
    //     console.log(evt)

    //     var files = evt.target.files; // FileList object
    //     console.log(files)
    //     var file = files[0];
    //     console.log(file)
    //     console.log(file.name)
    //     this.fileName = file.name;
    //    // this.fileToUpload = files.item(0);
    // }

    
    handleFile(files: FileList) {
        this.fileToUpload = files.item(0);
        console.log(this.fileToUpload);
        console.log(this.fileToUpload.name)
        this.fileName = this.fileToUpload.name;
    }

    importcsv() {
        this.spinStatus = true;
        this.btnSubmitStatus = false;
        this.chartService.importcsv(this.fileToUpload).subscribe(data => {
            alert("success")
            this.btnDownloadStatus = true;
            this.spinStatus = false;
            // this.reset();
        }, error => {
            alert('Upload Fail!, Please Check data column')
            this.spinStatus = false;
            // this.reset();
        });
        // this.reset();
    }
    exportcsv() {
        this.chartService.exportcsv().subscribe(res => {
            const blob = new Blob([res], { type: 'text/csv' });
            saveAs(blob, 'MalariaResult.csv')
           // this.btnSubmitStatus = true;
           this.reset()
        });
    }

    reset(){
        this.fileToUpload = undefined;
        this.fileName = "Choose file";
        this.btnSubmitStatus = true;
        this.btnDownloadStatus = false;
        
    }
}