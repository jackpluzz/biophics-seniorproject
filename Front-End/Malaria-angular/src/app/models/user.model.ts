export class UserRole {
    id: number;
    staffId: string;
    firstName: string;
    lastName: string;
    department: number;
    password: string;
    role: string;
}   