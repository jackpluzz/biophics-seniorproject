import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private ROOT_URL = 'http://localhost:8080';
  constructor(private http: HttpClient) {
  }

  getSpicies(): Observable<any> {
    return this.http.get(this.ROOT_URL + "/spicies")
  }

  getGender(): Observable<any> {
    return this.http.get(this.ROOT_URL + "/gender")
  }

  getMonth():Observable<any>{
    return this.http.get(this.ROOT_URL+"/monthdata")
  }

  importcsv(fileToUpload: any): Observable<any> {
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    return this.http.post(this.ROOT_URL + "/malaria/import", formData)
  }

  exportdatamodelcsv(): Observable<any> {
    return this.http.get(this.ROOT_URL + "/datamodel", {
      responseType: "blob"
    })
  }

  importdatamodelcsv(fileToUpload: any): Observable<any> {
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    return this.http.post(this.ROOT_URL + "/model/import", formData)
  }


  exportcsv(): Observable<any> {
    return this.http.get(this.ROOT_URL + "/malaria", {
      responseType: "blob"
    })
  }

  trainmodel(): Observable<any> {
    const herders = new HttpHeaders({
      "Content-Type": `application/json`
    });
    return this.http.post(this.ROOT_URL + "/malaria/buildmodel",{ responseType: 'json', headers: herders })
  }

  nukemodel(): Observable<any> {
    const herders = new HttpHeaders({
      "Content-Type": `application/json`
    });
    return this.http.delete(this.ROOT_URL + "/nukemodel",{ responseType: 'json', headers: herders })
  }





}
